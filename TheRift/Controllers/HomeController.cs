﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheRift.core;
using TheRift.Models;
using TheRift.DAL;
using Operations;

namespace TinyRift.Controllers {
    public class HomeController : Controller {

        ISite site = (ISite)App.Instance.Request.getItem("Site");
        IDbo DBO = (IDbo)App.Instance.Request.getItem("database");

        public ActionResult Index() {
            SiteOperations.loadPage(site,DBO,Constants.PageType.Home);        
            return View(site);
        }

        public ActionResult Page(int id) {
            return View(site);
        }

    }
}