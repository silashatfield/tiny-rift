﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PartialContentRow.aspx.cs" Inherits="System.Web.Mvc.ViewPage<List<TheRift.Models.PageContent>>" %>

<div class="row">
    <% foreach (PageContent con in Model) { %>
    <% GeneralContent page = con.Transform<GeneralContent>(); %>
        <div class="col-md-4">
            <h2><%: page.title %></h2>
            <p><%: page.content %></p>
            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
    <% } %>
</div>