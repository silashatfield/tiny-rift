﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Header.aspx.cs" Inherits="System.Web.Mvc.ViewPage<TheRift.Models.Site>" %>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <%--<a href="/"><img id="logo" src="@Url.Content("~/Images/brand.png")" /></a>--%>
        </div>

        <div id="navbar" class="navbar-collapse collapse">               
            <ul class="nav navbar-nav">
                <%--<li>@Html.ActionLink("Home", "Index", "Home")</li>
                <li>@Html.ActionLink("Blog", "Index", "Blog")</li>
                <li>@Html.ActionLink("About Me", "About", "Home")</li>
                <li>@Html.ActionLink("Portfolio", "Index", "Portfolio")</li>--%>
            </ul>
        </div>

        <div id="socialbar">
            <%--<a href="https://www.facebook.com/silas.hatfield"><img src="@Url.Content("~/Images/facebook.png")" /></a>
            <a href="https://twitter.com/spchatfield"><img src="@Url.Content("~/Images/twitter.png")" /></a>--%>
        </div>

    </div>
    <div class="bottomred"></div>
</nav>