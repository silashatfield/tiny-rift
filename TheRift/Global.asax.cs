﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using TheRift.core;

namespace TinyRift {
    public class Global : System.Web.HttpApplication {

        protected void Application_Start(object sender, EventArgs e) {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            App.Instance.Application_Start(sender, e);
        }

        protected void Session_Start(object sender, EventArgs e) {
            App.Instance.Session_Start(sender, e);
        }

        protected void Application_BeginRequest(object sender, EventArgs e) {
            App.Instance.Application_BeginRequest(sender, e);
        }

        protected void Application_EndRequest(object sender, EventArgs e) {
            App.Instance.Application_EndRequest(sender, e);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e) {
            App.Instance.Application_AuthenticateRequest(sender, e);
        }

        protected void Application_Error(object sender, EventArgs e) {
           App.Instance.Application_Error(sender, e);
        }

        protected void Session_End(object sender, EventArgs e) {
            App.Instance.Session_End(sender, e);
        }

        protected void Application_End(object sender, EventArgs e) {
            App.Instance.Application_End(sender, e);
        }
    }
}