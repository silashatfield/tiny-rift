﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Helpers {
    public class Debugger {
        private static readonly Debugger instance = new Debugger();

        private Debugger() { }

        public static Debugger Instance {
            get {
                return instance;
            }
        }
    }
}