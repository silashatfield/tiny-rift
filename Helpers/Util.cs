﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;

namespace TheRift.Helpers {
    public class Util {

    }

    //public static bool IsSimpleType(Type type)
    //{
    //    return
    //        type.IsPrimitive ||
    //        new Type[] {
    //            typeof(Enum),
    //            typeof(String),
    //            typeof(Decimal),
    //            typeof(DateTime),
    //            typeof(DateTimeOffset),
    //            typeof(TimeSpan),
    //            typeof(Guid)
    //        }.Contains(type) ||
    //        Convert.GetTypeCode(type) != TypeCode.Object ||
    //        (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>) && IsSimpleType(type.GetGenericArguments()[0]))
    //        ;
    //}

    public static class StringExtensions
    {
        public static string AppendToList(this string value, string item, string delim = ",")
        {
            if (string.IsNullOrEmpty(value)) value += item;
            else value += delim + item;
            return value;
        }
    }    

    public static class DataTableExtensions {
        public static IList<T> ToList<T>(this DataTable table) where T : new() {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            IList<T> result = new List<T>();

            foreach (var row in table.Rows) {
                var item = CreateItemFromRow<T>((DataRow)row, properties);
                result.Add(item);
            }

            return result;
        }

        public static IList<T> ToList<T>(this DataTable table, Dictionary<string, string> mappings) where T : new() {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            IList<T> result = new List<T>();

            foreach (var row in table.Rows) {
                var item = CreateItemFromRow<T>((DataRow)row, properties, mappings);
                result.Add(item);
            }

            return result;
        }

        public static T CreateItemFromRow<T>(DataRow row, IList<PropertyInfo> properties) where T : new() {
            T item = new T();
            foreach (var property in properties) {
                property.SetValue(item, row[property.Name], null);
            }
            return item;
        }

        public static T CreateItemFromRow<T>(DataRow row, IList<PropertyInfo> properties, Dictionary<string, string> mappings) where T : new() {
            T item = new T();
            foreach (var property in properties) {
                if (mappings.ContainsKey(property.Name.ToLower()))
                    property.SetValue(item, row[mappings[property.Name]], null);
            }
            return item;
        }

        public static T CreateItemFromRow<T>(this DataRow row) where T : new() {
            T item = new T();
            item.FillItemFromRow(row);
            return item;
        }

        public static void FillItemFromRow(this object obj, DataRow row) {
            var properties = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            foreach (var property in properties) {
                var flag = false;
                foreach (DataColumn col in row.Table.Columns) {
                    if (col.ColumnName.IndexOf(property.Name, StringComparison.Ordinal) > -1) flag = true;
                }
                if (flag && row[property.Name] != null) {
                    var val = row[property.Name];
                    if (val.GetType() == typeof(SByte)) {
                        property.SetValue(obj, Convert.ToBoolean(val), null);
                    } else {
                        property.SetValue(obj, (val == DBNull.Value) ? null : val, null);
                    }
                }
            }
        }

        public static void FillListFromRows<T>(this IList obj, DataRowCollection rows) where T : new() {    
               
            foreach (DataRow row in rows) {
                T item = new T();
                item.FillItemFromRow(row);
                obj.Add(item);
            }

        }
        
        public static void CreateItem<T>(this DataRow row, object i){
            var properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            foreach (var property in properties) {
                if (row[property.Name] != null) {
                    var val = row[property.Name];
                    if (val.GetType() == typeof(SByte)) {
                        property.SetValue(i, Convert.ToBoolean(val), null);
                    } else {
                        property.SetValue(i, (val == DBNull.Value) ? null : val, null);
                    }    
                }
            }
        }
    }
}