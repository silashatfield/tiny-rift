﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace TheRift.Helpers {
    public class Serial {
        private static readonly Serial instance = new Serial();
        private static string _filePath = @"..\tests\data\";

        private Serial() { }

        public static Serial Instance {
            get {
                return instance;
            }
        }

        public static string filePath {
            get {
                return _filePath;
            }
            set {
                _filePath = value;
            }
        }

        public static void WriteToBinaryFile<T>(string fileName, T objectToWrite) {
            using (Stream stream = File.Open(Serial.filePath + fileName,FileMode.Create, FileAccess.Write)) {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }
        
        public static T ReadFromBinaryFile<T>(string fileName) {
            using (Stream stream = File.Open(Serial.filePath + filePath, FileMode.Open, FileAccess.Read)) {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return (T)binaryFormatter.Deserialize(stream);
            }
        }

    }
}