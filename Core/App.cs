﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using TheRift.Models;
using TheRift.Helpers;
using TheRift.DAL;

namespace TheRift.core {
    public class App {
        private static readonly App _instance = new App();
        private static string _databasetype;

        private App() {
            Config = new DefaultConfig();
        }

        public static App Instance {
            get {
                return _instance;
            }
        }       

        public IRequest Request { get; set; }

        public static IConfig Config { get; set; }

        public static string DatabaseType {
            get {
                if (_databasetype != null) {
                    return _databasetype;
                }
                try {
                    return Config.getSettingValue("database"); //TODO: move this to a helper
                } catch(Exception e) {
                    return _databasetype;
                }
            }
            set {
                _databasetype = value;
            }
        }

        public void Application_Start(object sender, EventArgs e) {
            Request = (IRequest)new Request();

            IDbo DBO = new TheRift.DAL.MySQL();
            Request.setItem("Database", DBO);
            DBO.connect();

            Constants.SiteModes = new SiteMode().loadby<SiteMode>();
            Constants.PageTypes = new PageType().loadby<PageType>();
            Constants.UserTypes = new UserType().loadby<UserType>();

            DBO.close();

        }

        public void Session_Start(object sender, EventArgs e) {

        }

        public void Application_BeginRequest(object sender, EventArgs e) {
            //IDbo DBO = (IDbo)Activator.CreateInstance(Type.GetType("TheRift.DAL." + DatabaseType));
            IDbo DBO = new TheRift.DAL.MySQL();
            Request.setItem("Database", DBO);
            DBO.connect();

            ISite site = new Site();
            site = Operations.SiteOperations.loadSite(Request.domain,DBO);            
            Request.setItem("Site", site);  

        }

        public void Application_EndRequest(object sender, EventArgs e) {
            IDbo DBO = (IDbo)Request.getItem("Database");
            DBO.close();
        }

        public void Application_AuthenticateRequest(object sender, EventArgs e) {

        }

        public void Application_Error(object sender, EventArgs e) {

        }

        public void Session_End(object sender, EventArgs e) {

        }

        public void Application_End(object sender, EventArgs e) {
            
        }

    }
}