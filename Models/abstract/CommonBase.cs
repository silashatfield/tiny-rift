﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models
{
	public class CommonBase : Base {

        private IRequest request = new Request();

        protected IDbo DBO {
            get { return (IDbo)Request.getItem("Database"); }
        }

        protected IRequest Request {
            get { return request; }
        }

        public bool IsNewRecord
        {
            get { return this.id == 0;  }
        }

        public CommonBase() : base() {
            request = null; // App.Instance.Request;
            //DBO = (IDbo)request.getItem("Database");
        }

    }
}