﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace TheRift.Models
{
	public class MetaBase : CommonBase {

        public List<Meta> meta { get; set; }

        public MetaBase() : base() {

        }
        public T GetMeta<T>(string key)
        {
            object result = null;
            Type type = typeof(T);
            Meta m = meta.FirstOrDefault(x => x.meta_key == key);

            if (type.IsEnum)
            {
                result = Enum.Parse(typeof(T), m.meta_textvalue);
            }
            if (type == typeof(bool))
            {
                result = m.meta_boolvalue;
            }
            else if (type == typeof(string))
            {
                result = m.meta_textvalue;
            }
            else if (type == typeof(DateTime))
            {
                result = m.meta_datevalue;
            }
            else if (type == typeof(int))
            {
                result = m.meta_intvalue;
            }
            else if (type == typeof(decimal))
            {
                result = m.meta_decimalvalue;
            }

            return (T)result;
        }

        public void SetMeta(string key, object val)
        {
            Type type = val.GetType();

            Meta m = meta.FirstOrDefault(x => x.meta_key == key);
            if (m == null)
            {
                m = new Meta();
                m.metaobj_type = Constants.TableNames.PageContent.ToDescription(); //TODO make this an arg
                m.metaobj_id = this.id;
                this.meta.Add(m);
            }
            if (type.IsEnum)
            {
                m.meta_textvalue = ((Enum)val).ToDescription();
            }
            if (type == typeof(bool))
            {
                m.meta_boolvalue = (bool)val;
            }
            else if (type == typeof(string))
            {
                m.meta_textvalue = (string)val;
            }
            else if (type == typeof(DateTime))
            {
                m.meta_datevalue = (DateTime)val;
            }
            else if (type == typeof(int))
            {
                m.meta_intvalue = (int)val;
            }
            else if (type == typeof(decimal))
            {
                m.meta_decimalvalue = (decimal)val;
            }
        }

        public T Transform<T>() where T : new()
        {
            var trans = new T();
            var properties = trans.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            foreach (var property in properties)
            {
                var type = property.PropertyType;
                var thistype = this.GetType();
                var val = thistype.GetMethod("GetMeta").MakeGenericMethod(type).Invoke(this, new object[] { property.Name });
                property.SetValue(trans, val);
            }
            return (T)trans;
        }

        public void UpdateMeta(object obj)
        {
            var properties = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            foreach (var property in properties)
            {
                this.SetMeta(property.Name, property.GetValue(obj));
            }
        }


    }
}