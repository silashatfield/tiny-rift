﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

namespace TheRift.Models {

    public static class EnumExtensions
    {
        public static string ToDescription(this Enum value)
        {
            var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return da.Length > 0 ? da[0].Description : value.ToString();
        }

        //public static string ToEnumDescription<T>(this object value)
        //{
        //    T enumVal = (T)Enum.Parse(typeof(T), ((Enum)value).ToDescription.ToString());
        //        return enumVal;
        //    }
        //var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
        //    return da.Length > 0 ? da[0].Description : value.ToString();
        //}
    }

    public class Constants
    {

        public static List<SiteMode> SiteModes
        {
            get; set;
        }

        public static List<Models.PageType> PageTypes
        {
            get; set;
        }

        public static List<UserType> UserTypes
        {
            get; set;
        }

        public enum QueryType
        {
            SiteLoad
            , DomainsLoad
            , SiteModesLoad
            , PageTypesLoad
            , UserTypesLoad
            , PageLoad
            , ContentsLoad
        }

        public enum PageType
        {
            None,
            Home
        }

        public enum TableNames
        {
            [Description("site_sites")]
            Site,
            [Description("lookup_sitemodes")]
            SiteMode,
            [Description("site_domains")]
            SiteDomain,
            [Description("lookup_pagetypes")]
            PageType,
            [Description("lookup_usertypes")]
            UserType,
            [Description("page_pages")]
            Page,
            [Description("page_sections")]
            PageSection,
            [Description("page_contents")]
            PageContent,
            [Description("meta")]
            Meta
        }

        public enum ContentSection
        {
            [Description("PartialContentRow")]
            PartialContentRow,
            [Description("Jumbotron")]
            Jumbotron
        }

        public enum ContentMetaField
        {
            [Description("title")]
            title,
            [Description("content")]
            content,
            [Description("created_by")]
            created_by,
            [Description("updated_by")]
            updated_by,
            [Description("created_date")]
            created_date,
            [Description("updated_date")]
            updated_date,
            [Description("content_section")]
            content_section,
            [Description("parent_meta_id")]
            parent_meta_id,
            [Description("image")]
            image
        }
    }
}