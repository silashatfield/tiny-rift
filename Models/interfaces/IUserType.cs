﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public interface IUserType {
        int id { get; set; }
        string name { get; set; }
        string description { get; set; }
        int bitmask { get; set; }
        int permissionbitmask { get; set; }
    }
}