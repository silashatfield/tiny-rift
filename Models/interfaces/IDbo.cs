﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TheRift.Models;

namespace TheRift.Models {
    public interface IDbo {
        int nonquery(string q, Hashtable v = null);
        MySqlConnection Conn { get; }
        T load<T>(string table);
        T load<T>(string proc, Hashtable args);
        DataSet query(Constants.QueryType type, Hashtable v = null);
        DataSet query(string q, Hashtable v = null);
        void connect();
        void close();
    }
}