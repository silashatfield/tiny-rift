﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public interface IPageType {

        int id { get; set; }
        string name { get; set; }
        string description { get; set; }

    }
}