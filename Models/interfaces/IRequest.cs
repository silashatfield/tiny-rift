﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public interface IRequest {
        string domain { get; }
        void setSessionValue(string key, object val);
        object getSessionValue(string key);
        object getItem(string key);
        void setItem(string key, object val);
    }
}