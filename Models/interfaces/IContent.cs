﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public interface IContent {

        int id { get; set; }
        string content { get; set; }
        int page_id { get; set; }
        DateTime date_created { get; set; }
        DateTime date_updated { get; set; }
        int created_by { get; set; }
        int updated_by { get; set; }
        bool active { get; set; }

    }
}