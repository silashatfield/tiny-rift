﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public interface ISite {
        int id { get; set; }
        string route { get; set; }
        string site_name { get; set; }
        bool active { get; set; }
        Page page { get; set; }
        string stylesheet { get; }
        string favico { get; }
        string description { get; }
        string company_name { get; set; }
        string title { get; set; }
    }
}