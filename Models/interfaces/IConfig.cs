﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheRift.Models {
    public interface IConfig {
       string getSettingValue(string key);
       void setSettingValue(string key, string value);
       string getConnectionValue(string key);
       void setConnectionValue(string key, string value);
    }
}
