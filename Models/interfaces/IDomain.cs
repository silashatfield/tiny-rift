﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public interface IDomain {

        int id { get; set; }
        string domain { get; set; }
        int site_id { get; set; }
        int site_mode_id { get; set; }
        bool active { get; set; }

    }
}