﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public interface IPage {

        int id { get; set; }
        List<IContent> contents { get; set; }
        int site_id { get; set; }
        int page_type_id { get; set; }
        bool active { get; set; }

    }
}