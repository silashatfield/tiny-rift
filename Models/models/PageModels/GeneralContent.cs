﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models
{
    public class GeneralContent
    {
        public string title { get; set; }
        public string content { get; set; }
        public int created_by { get; set; }
        public int updated_by { get; set; }
        public DateTime created_date { get; set; }
        public DateTime updated_date { get; set; }        
    }
}