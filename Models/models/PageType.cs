﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public class PageType : CommonBase,IPageType {

        public string name { get; set; }
        public string description { get; set; }
        public PageType() {
            db_tablename = Constants.TableNames.PageType.ToDescription();
        }
    }
}