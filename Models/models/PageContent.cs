﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace TheRift.Models {
    public class PageContent : MetaBase
    {

        public int page_id { get; set; }
        public bool active { get; set; }

        public PageContent() : base()
        {
            this.db_tablename = Constants.TableNames.PageContent.ToDescription();
        }

    }
}