﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace TheRift.Models {
    public class PageSection : MetaBase
    {
        public List<PageContent> contents { get; set; }
        public int page_id { get; set; }
        public bool active { get; set; }        

        public PageSection() : base()
        {
            this.db_tablename = Constants.TableNames.PageSection.ToDescription();
        }        

    }
}