﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public class Page : MetaBase {
        public List<PageSection> sections { get; set; }
        public int site_id { get; set; }
        public int page_type_id { get; set; }
        public bool active { get; set; }

        public Page() : base()
        {
            this.db_tablename = Constants.TableNames.Page.ToDescription();
        }

        public List<PageSection> ContentSection(Constants.ContentSection section)
        {
            List<PageSection> result = this.sections.Where(c=>c.meta.Any(m=>m.meta_key == "content_section" && m.meta_textvalue == section.ToDescription()))?.ToList();
            if (result == null) result = new List<PageSection>();
            return result;
        }
    }
}

