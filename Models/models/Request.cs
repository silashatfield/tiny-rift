﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public class Request : IRequest {

        public string domain {
            get {
                return HttpContext.Current.Request.Url.Host;
            }
        }

        public object getSessionValue(string key) {
            return HttpContext.Current.Session[key];
        }

        public void setSessionValue(string key, object val) {
            HttpContext.Current.Session[key] = val;
        }

        public object getItem(string key) {
            return HttpContext.Current.Items[key];
        }

        public void setItem(string key, object val) {
            HttpContext.Current.Items[key] = val;
        }

    }
}