﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public class UserType : CommonBase, IUserType {
        
        public string name { get; set; }
        public string description { get; set; }
	    public int bitmask { get; set; }
	    public int permissionbitmask { get; set; }

        public UserType() {
            db_tablename = Constants.TableNames.UserType.ToDescription();
        }
    }
}