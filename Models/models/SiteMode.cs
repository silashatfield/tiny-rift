﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public class SiteMode : CommonBase, ISiteMode {

        public string name { get; set; }
        public string description { get; set; }

        public SiteMode() {
            db_tablename = Constants.TableNames.SiteMode.ToDescription();
        }
    }
}