﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public class Site : CommonBase, ISite {

        public string route { get; set; }
        public string site_name { get; set; }
        public string company_name { get; set; }
        public bool active { get; set; }
        public List<Domain> domains { get; set; }
        public Page page { get; set; }
        public string title { get; set; }
        public string stylesheet
        {
            get
            {
                return "Sites/" + this.id.ToString() + "/Styles/style.css";
            }
        }

        public string favico {
            get {
                return "Sites/" + this.id.ToString() + "/Images/favicon.ico";
            }
        }

        public string description {
            get {
                return "TODO: ADD THIS";
            }
        }

        public Site() : base()
        {
            this.db_tablename = Constants.TableNames.Site.ToDescription();
        }      

    }
}