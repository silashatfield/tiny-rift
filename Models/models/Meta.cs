﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public class Meta : CommonBase {

        public string metaobj_type { get; set; }
	    public int metaobj_id { get; set; }
        public string meta_key { get; set; }
        public string meta_textvalue {get; set; }
        public int meta_intvalue { get; set; }
	    public decimal meta_decimalvalue { get; set; }
	    public bool meta_boolvalue { get; set; }
        public DateTime meta_datevalue { get; set; }

        public Meta() : base()
        {
            this.db_tablename = Constants.TableNames.Meta.ToDescription();
        }

    }

}