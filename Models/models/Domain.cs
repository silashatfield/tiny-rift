﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheRift.Models {
    public class Domain : CommonBase, IDomain {

        public string domain { get; set; }
        public int site_id { get; set; }
        public int site_mode_id { get; set; }
        public bool active { get; set;  }

        public Domain() : base()
        {
            this.db_tablename = Constants.TableNames.SiteDomain.ToDescription();
        }

    }
}