﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheRift.Helpers;
using TheRift.Models;
using TheRift.DAL;
using MySql.Data;

namespace Operations {
    public static class SiteOperations {

        public static void loadPage(ISite site, IDbo DBO, Constants.PageType type)
        {
            //LOAD PAGE
            var v = new Hashtable();
            v.Add("site_id", site.id);
            v.Add("page_type_id", (int)type);
            site.page = new TheRift.Models.Page();
            site.page.loadby(v);

            //LOAD SECTION
            v = new Hashtable();
            v.Add("page_id", site.page.id);
            site.page.sections = new PageSection().loadby<PageSection>(v);
            
            foreach (PageSection section in site.page.sections)
            {
                //LOAD SECTION META
                v = new Hashtable();
                v.Add("metaobj_type", Constants.TableNames.PageSection.ToDescription());
                v.Add("metaobj_id", section.id);
                section.meta = new Meta().loadby<Meta>(v);

                //LOAD SECTION CONTENT
                v = new Hashtable();
                v.Add("section_id", site.page.id);
                section.contents = new PageContent().loadby<PageContent>(v);

                //LOAD CONTENT META
                foreach (PageContent con in section.contents)
                {
                    v = new Hashtable();
                    v.Add("metaobj_type", Constants.TableNames.PageContent.ToDescription());
                    v.Add("metaobj_id", con.id);
                    con.meta = new Meta().loadby<Meta>(v);
                }
            }
        }

        public static TheRift.Models.Site loadSite(string domain, IDbo DBO)
        {
            var v = new Hashtable();
            var site = new TheRift.Models.Site();

            v.Add("domain", domain);
            var thisdomain = new TheRift.Models.Domain();
            thisdomain.loadby(v);

            if (!thisdomain.IsNewRecord) {
                site.load(thisdomain.site_id);

                v = new Hashtable();
                v.Add("site_id", site.id);
                site.domains = thisdomain.loadby<Domain>(v);                
            } else {
                //todo redirect
            }

            return site;
        }

    }
}