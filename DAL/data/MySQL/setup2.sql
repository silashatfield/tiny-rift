CREATE DATABASE IF NOT EXISTS rift;

USE rift;

CREATE TABLE IF NOT EXISTS data_lookups (
	id int,
    name varchar(255) not null,
    [group] varchar(255) not null,
	bitmask int    
);
INSERT INTO data_lookups (id,name,[group],bitmask) VALUES 
(1,'parent','relationshiptype',null)
,(2,'key','relationshiptype',null)


,(20,'int','datatype',null)
,(21,'string','datatype',null)
,(22,'decimal','datatype',null)
,(23,'datetime','datatype',null)
,(24,'array','datatype',null);






CREATE TABLE IF NOT EXISTS lookup_sitemodes (
	id int Auto_Increment Primary Key,
    name varchar(255) not null,
    description varchar(255)    
);
INSERT INTO lookup_sitemodes (name,description) VALUES 
('local','local development')
,('test','testing')
,('production','live ');

CREATE TABLE IF NOT EXISTS lookup_plugins (
	id int Auto_Increment Primary Key,
    name varchar(255),
    description varchar(255),
	class varchar(255),
    active tinyint default 1 
);

CREATE TABLE IF NOT EXISTS lookup_pagetypes (
	id int Auto_Increment Primary Key,
    name varchar(255) not null,
    description varchar(255)
);
INSERT INTO lookup_pagetypes (name,description) VALUES 
('Home','Home Page')
,('Portfolio','Portfolio Page')
,('Contact','Contact Page')
,('Resume','Resume Page');

CREATE TABLE IF NOT EXISTS lookup_usertypes (
	id int Auto_Increment Primary Key,
    name varchar(255) not null,
    description varchar(255),
	bitmask int,
	permissionbitmask int default 0
);
INSERT INTO lookup_usertypes (name,description,bitmask) VALUES 
('Global Admin','Global Administrator', 1)
,('Client Admin','Client Admin', 2)
,('Client User','Client', 4);

CREATE TABLE IF NOT EXISTS site_sites (
	id int Auto_Increment Primary Key,
    route varchar(255),
    site_name varchar (255) not null,
	company_name varchar(255),
    active tinyint default 1
);
INSERT INTO site_sites (site_name) VALUES ('riftwebdesign.com');

CREATE TABLE IF NOT EXISTS site_domains (
	id int Auto_Increment Primary Key,
    domain varchar(255) not null,
    site_id int not null,
    site_mode_id int not null,
	active tinyint default 1,
    FOREIGN KEY (site_id) REFERENCES site_sites(id) ON DELETE CASCADE,
	FOREIGN KEY (site_mode_id) REFERENCES lookup_sitemodes(id)
);
INSERT INTO site_domains (domain,site_id,site_mode_id) VALUES 
('local.riftwebdesign.com',1,1)
,('test.riftwebdesign.com',1,2)
,('riftwebdesign.com',1,3);

CREATE TABLE IF NOT EXISTS page_pages (
	id int Auto_Increment Primary Key,    
    site_id int not null,
    page_type_id int not null,
	active tinyint default 1,
    FOREIGN KEY (site_id) REFERENCES site_sites(id) ON DELETE CASCADE,
	FOREIGN KEY (page_type_id) REFERENCES lookup_pagetypes(id)
);
INSERT INTO page_pages (site_id,page_type_id) VALUES 
(1,1);

CREATE TABLE IF NOT EXISTS page_sections (
	id int Auto_Increment Primary Key,  
    page_id int not null,
	active tinyint default 1,
    FOREIGN KEY (site_id) REFERENCES site_sites(id) ON DELETE CASCADE,
	FOREIGN KEY (page_type_id) REFERENCES lookup_pagetypes(id)
);
INSERT INTO page_pages (site_id,page_type_id) VALUES 
(1,1);

CREATE TABLE IF NOT EXISTS page_content (
	id int Auto_Increment Primary Key, 
    section_id int not null,
	active tinyint default 1,
    FOREIGN KEY (page_id) REFERENCES page_pages(id) ON DELETE CASCADE
);
INSERT INTO page_content (page_id) VALUES 
(1);

CREATE TABLE IF NOT EXISTS meta (
	id int Auto_Increment Primary Key, 
	metaobj_type varchar(255) not null,
	metaobj_id int not null,
	meta_key varchar(255) not null,
	meta_textvalue LongText,
	meta_intvalue int,
	meta_decimalvalue decimal(16,4),
	meta_boolvalue tinyint,
	meta_datevalue datetime
);
INSERT INTO meta (metaobj_type,metaobj_id,meta_key,meta_textvalue,meta_intvalue,meta_datevalue) VALUES 
('page_content',1,'title','This is a test page content',NULL,NULL)
,('page_content',1,'content','Test page! Content',NULL,NULL)
,('page_content',1,'created_by',NULL,1,NULL)
,('page_content',1,'updated_by',NULL,1,NULL)
,('page_content',1,'created_date',NULL,NULL,'2016-08-06')
,('page_content',1,'updated_date',NULL,NULL,'2016-08-06')
,('page_content',1,'content_section','PartialDescription',NULL,NULL);

CREATE TABLE IF NOT EXISTS user_users (
	id int Auto_Increment Primary Key,    
    site_id int not null,
	permission_bitmask int not null default 0,
	group_bitmask int not null default 2,
	username varchar(50) not null,
	password varchar(255) not null,
	active tinyint default 1,
    FOREIGN KEY (site_id) REFERENCES site_sites(id) ON DELETE CASCADE
);
INSERT INTO user_users(site_id,group_bitmask,username,password)
VALUES
(1,1,'System','nologin')
,(1,1,'shatfield','801345');







