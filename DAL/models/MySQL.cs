﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using TheRift.Models;
using TheRift.Helpers;
using System.Reflection;

namespace TheRift.DAL {

    public static class MySQLExtensions
    {
        private static IRequest request = new Request();

        public static void load(this CommonBase obj, int id)
        {
            IDbo dbo = (IDbo)request.getItem("Database");
            MySqlCommand cmd = dbo.Conn.CreateCommand();
            if (string.IsNullOrEmpty(obj.db_tablename)) throw new Exception("No Tablename Provided for the object");
            if (id == 0) throw new Exception("Cannot load a new object");

            Hashtable v = new Hashtable() { { "@id", id } };
            var sql = "select * from " + obj.db_tablename + " where id=@id limit 0,1";
            DataSet ds = dbo.query(sql, v);

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                var row = ds.Tables[0].Rows[0];
                obj.FillItemFromRow(row);
            }
        }

        public static List<T> loadby<T>(this CommonBase obj, Hashtable v = null) where T : new() {
            var ds = loadby(obj, v, true);
            var list = new List<T>();            
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                var rows = ds.Tables[0].Rows;
                list.FillListFromRows<T>(rows);
            }
            return list;
        }

        public static void loadby(this CommonBase obj, Hashtable v = null) {
            var ds = loadby(obj,v,false);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
                var row = ds.Tables[0].Rows[0];
                obj.FillItemFromRow(row);
            }
        }

        private static DataSet loadby(CommonBase obj, Hashtable v = null, bool MoreThanOne = false) {
            IDbo dbo = (IDbo)request.getItem("Database");
            MySqlCommand cmd = dbo.Conn.CreateCommand();
            if (string.IsNullOrEmpty(obj.db_tablename)) throw new Exception("No Tablename Provided for the object");

            var searchlist = "1=1";
            Hashtable args = null;
            if(v != null) {
                args = new Hashtable();
                foreach (DictionaryEntry Item in v) {
                    var param = "@" + Item.Key.ToString();
                    searchlist = searchlist.AppendToList(Item.Key.ToString() + " = " + param, " and ");
                    args.Add(param, Item.Value);
                }
            }
                

            var sql = string.Format("select * from {0} where {1} {2}", obj.db_tablename, searchlist, MoreThanOne ? "" : "limit 0,1");
            setquery(obj, sql, v);
            DataSet ds = dbo.query(sql, args);

            return ds;
        }

        public static void delete(this CommonBase obj) {
            IDbo dbo = (IDbo)request.getItem("Database");
            MySqlCommand cmd = dbo.Conn.CreateCommand();
            if (string.IsNullOrEmpty(obj.db_tablename)) throw new Exception("No Tablename Provided for the object");
            if (obj.id == 0) throw new Exception("Cannot load a new object");

            Hashtable v = new Hashtable() { { "@id", obj.id } };
            var sql = "delete from " + obj.db_tablename + " where id=@id";
            dbo.nonquery(sql, v);
        }

        private static void setquery(CommonBase obj, string q, Hashtable v = null) {
            obj.query = q;
            if (v != null) {
                foreach (DictionaryEntry Item in v) {
                    obj.query = obj.query.Replace(Item.Key.ToString(),"'"+Item.Value.ToString()+"'");
                }                
            }
        }

        public static void save(this CommonBase obj)
        {
            IDbo dbo = (IDbo)request.getItem("Database");
            MySqlCommand cmd = dbo.Conn.CreateCommand();
            Base b = ((Base)obj);
            if (string.IsNullOrEmpty(b.db_tablename)) throw new Exception("No Tablename Provided for the object");

            if(obj.IsNewRecord)
            {
                //save logic
                Hashtable v = new Hashtable();
                var sql = "select * from " + b.db_tablename + " limit 0,1";
                DataSet ds = dbo.query(sql, v);
                var insertlist = "";
                var valuelist = "";

                var properties = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
                foreach (DataColumn col in ds.Tables[0].Columns)
                {
                    var prop = properties.FirstOrDefault(x => x.Name.IndexOf(col.ColumnName, StringComparison.Ordinal) > -1);
                    if(prop != null) {                        
                        var val = prop.GetValue(obj);
                        object value;
                        if (val.GetType() == typeof(bool)) {
                            value = ((bool)val) ? 1 : 0;
                        } else {
                            value = val;
                        }
                        if (value != null) {
                            var param = "@" + col.ColumnName;
                            insertlist = insertlist.AppendToList(col.ColumnName);
                            valuelist = valuelist.AppendToList(param);
                            v.Add(param,value);
                        }
                    }                    
                }
                sql = string.Format("insert into {0} ( {1} ) values ( {2} )", b.db_tablename, insertlist, valuelist);
                obj.id = dbo.nonquery(sql, v);
            }
            else
            {
                //update logic
                Hashtable v = new Hashtable();
                var sql = "select * from " + b.db_tablename + " limit 0,1";
                DataSet ds = dbo.query(sql, v);
                var updatelist = "";

                var properties = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
                foreach (DataColumn col in ds.Tables[0].Columns) {
                    var prop = properties.FirstOrDefault(x => x.Name.IndexOf(col.ColumnName, StringComparison.Ordinal) > -1);
                    if (prop != null) {
                        var val = prop.GetValue(obj);
                        object value;
                        if (val.GetType() == typeof(bool)) {
                            value = ((bool)val) ? 1 : 0;
                        } else {
                            value = val;
                        }
                        if (value != null) {
                            var param = "@" + col.ColumnName;
                            updatelist = updatelist.AppendToList(col.ColumnName+"="+param);
                            v.Add(param, value);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(updatelist)) {
                    sql = string.Format("update {0} set {1}", b.db_tablename, updatelist);
                    dbo.nonquery(sql, v);
                }                
            }

            
        }
    }

    public class MySQL : IDbo {
        private MySqlConnection conn;
        private string connectionString = "server=localhost;port=3306;userid=sid;password=1scrappie;"; // App.Config.getConnectionValue("default");
        private string scriptsFolder = System.AppDomain.CurrentDomain.BaseDirectory+@"..\dal\data\MySQL\";        

        public MySQL() {

        }

        public MySqlConnection Conn
        {
            get
            {
                return conn;
            }
        }

        public void connect() {
            this.conn = new MySqlConnection(this.connectionString);
            this.conn.Open();
            MySqlCommand cmd = this.conn.CreateCommand();
            try {
                cmd.CommandText = "use rift";
                cmd.ExecuteNonQuery();
            } catch (MySqlException e) {
                MySqlScript script = new MySqlScript(this.conn, File.ReadAllText(this.scriptsFolder + "setup.sql"));
                script.Execute();
                cmd.CommandText = "use rift";
                cmd.ExecuteNonQuery();
            }
        }

        public void close() {
            this.conn.Close();
        }

        public void filter<T>(Hashtable args) {
            
        }

        //public T load<T>(object obj) where T : new() {
        //    Base b = ((Base)obj);
        //    if (string.IsNullOrEmpty(b.db_tablename)) throw new Exception("No Tablename Provided for the object");
        //    if (b.id == 0) throw new Exception("Cannot load a new object");

        //    Hashtable v = new Hashtable() { { "@id", b.id } };
        //    var sql = "select * from " + b.db_tablename + " where id=@id limit 0,1";
        //    DataSet ds = query(sql,v);

        //    if(ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0) {
        //        var row = ds.Tables[0].Rows[0];
        //        return row.CreateItemFromRow<T>();
        //    }

        //    return (T)obj;
        //}

        public T load<T>(string table) {
            var sql = "select * from " + table;
            DataSet ds = query(sql);
            return (T)Activator.CreateInstance(typeof(T), ds);
        }

        public T load<T>(string proc, Hashtable args = null) {           
            MySqlCommand cmd = new MySqlCommand(proc, this.conn);
            DataSet ds = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;            
            if (args != null) {
                foreach (DictionaryEntry Item in args) {
                    cmd.Parameters.AddWithValue(Item.Key.ToString(), Item.Value.ToString());
                }
                if (!args.ContainsKey("relations")) {
                    cmd.Parameters.AddWithValue("relations", "");
                }
            }
            MySqlDataAdapter da;
            da = new MySqlDataAdapter(cmd);
            da.Fill(ds);
            return (T)Activator.CreateInstance(typeof(T), ds);
        }

        public int save(string proc, Hashtable args = null) {
            MySqlCommand cmd = new MySqlCommand(proc, this.conn);
            if (args != null) {
                foreach (DictionaryEntry Item in args) {
                    cmd.Parameters.AddWithValue(Item.Key.ToString(), Item.Value.ToString());
                }
            }
            cmd.ExecuteNonQuery();
            return Convert.ToInt32(cmd.LastInsertedId);
        }

        public DataSet query(string q, Hashtable v = null) {
            MySqlCommand cmd = this.conn.CreateCommand();
            cmd.CommandText = q;
            if (v != null) {
                fillCommand(cmd, v);
            }

#if DEBUG
            Console.Write(q);
#endif

            cmd.ExecuteNonQuery();
            DataSet ds = new DataSet();
            using (MySqlDataAdapter adapter = new MySqlDataAdapter(cmd)) {
                adapter.Fill(ds);
            };
            return ds;
        }

        public int nonquery(string q, Hashtable v = null) {
            MySqlCommand cmd = this.conn.CreateCommand();
            cmd.CommandText = q;
            if (v != null) {
                fillCommand(cmd, v);
            }
#if DEBUG
            Console.Write(q);
#endif
            cmd.ExecuteNonQuery();
            return (int)cmd.LastInsertedId;
        }

        private void fillCommand(MySqlCommand cmd, Hashtable v) {
            foreach (DictionaryEntry Item in v) {
                cmd.Parameters.AddWithValue(Item.Key.ToString(), Item.Value.ToString());
            }
            cmd.Prepare();
        }

        public DataSet query(Constants.QueryType type, Hashtable v = null) {
            return query(getQuery(type), v);
        }

        private string getQuery(Constants.QueryType type) {
            string result = "";
            switch(type) {
                case Constants.QueryType.SiteLoad:
                    result = "select s.* from " + Constants.TableNames.Site.ToDescription() + " s inner join " + Constants.TableNames.SiteDomain.ToDescription() + " sd on sd.site_id = s.id where sd.domain = @domain" ;
                    break;
                case Constants.QueryType.DomainsLoad:
                    result = "select * from " + Constants.TableNames.SiteDomain.ToDescription() + " where site_id = @site_id";
                    break;
                case Constants.QueryType.SiteModesLoad:
                    result = "select * from " + Constants.TableNames.SiteMode.ToDescription();
                    break;
                case Constants.QueryType.PageTypesLoad:
                    result = "select * from " + Constants.TableNames.PageType.ToDescription();
                    break;
                case Constants.QueryType.UserTypesLoad:
                    result = "select * from " + Constants.TableNames.UserType.ToDescription();
                    break;
                case Constants.QueryType.PageLoad:
                    result = "select * from " + Constants.TableNames.Page.ToDescription() + " where page_type_id = @page_type_id and site_id = @site_id";
                    break;
                case Constants.QueryType.ContentsLoad:
                    result = "select * from " + Constants.TableNames.PageContent.ToDescription() + " where page_id = @page_id";
                    break;
            }
            return result;
        }
                
    }
}